package com.blackhawk.entrypoint;

import com.blackhawk.modelSupport.FamiliyTreeOperations;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Slf4j
public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            log.error("Expected file location as argument");
            System.exit(1);
        }

        Path path = Paths.get(args[0]);

        try (Stream<String> stream = Files.lines(path)){
            stream.forEach(
                    FamiliyTreeOperations::parseCommandForKingShanTree
            );
        } catch (IOException e) {
            log.error("Failed reading input file: {}",path.getFileName() );
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }
}
