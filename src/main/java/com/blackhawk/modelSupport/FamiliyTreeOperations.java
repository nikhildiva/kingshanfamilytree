package com.blackhawk.modelSupport;

import com.blackhawk.factory.query.Query;
import com.blackhawk.factory.query.QueryFactory;
import com.blackhawk.familytree.KingShanFamilyTree;
import com.blackhawk.model.Person;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FamiliyTreeOperations {
    public static void parseCommandForKingShanTree(String command) {
        String[] brokenCommand = command.split(" ");
        Person allFather = KingShanFamilyTree.allFather;

        Query query = QueryFactory.getQuery(brokenCommand[0]);
        if (query != null) {
            System.out.println(query.performQuery(allFather, brokenCommand));
        }
    }
}
