package com.blackhawk.modelSupport;

import com.blackhawk.constant.Gender;
import com.blackhawk.model.Person;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class Persons {

    private Persons() {
    }

    private static final class EmptyPerson extends Person {
        public EmptyPerson() {
            super("Empty", Gender.MALE);
        }

        @Override
        public Person getFather() {
            return this;
        }

        @Override
        public Person getMother() {
            return this;
        }

        @Override
        public Person getSpouse() {
            return this;
        }

        @Override
        public String toString() {
            return "I am an non-existent";
        }
    }

    private static final Person EMPTY_PERSON = new EmptyPerson();


    public static Person emptyPerson() {
        return EMPTY_PERSON;
    }

    public static boolean isEmpty(Person person) {
        return EMPTY_PERSON == person;
    }

    public static Stream<Person> getSiblingsAsStream(Person person) {
        if (Persons.isEmpty(person.getFather())) {
            return Stream.empty();
        }
        List<Person> siblings = person
                .getFather()
                .getChildren();

        return siblings.stream()
                .filter(sibling -> sibling != person && !Persons.isEmpty(sibling));
    }

    public static Person findInTree(Person allFather, String name) {
        Set<Person> checked = new HashSet<>();
        checked.add(EMPTY_PERSON);
        return searchPerson(allFather, name, checked);
    }

    private static Person searchPerson(Person person, String searchName, Set<Person> checkSet) {
        if (!checkSet.contains(person)) {
            checkSet.add(person);
            Person spouse = person.getSpouse();
            List<Person> children = person.getChildren();
            if (person.getName().equals(searchName)) {
                return person;
            } else {
                Person tempPerson = searchPerson(spouse, searchName, checkSet);
                if (tempPerson != emptyPerson()) {
                    return tempPerson;
                }

                for (Person child : children) {
                    tempPerson = searchPerson(child, searchName, checkSet);
                    if (tempPerson != emptyPerson()) {
                        return tempPerson;
                    }
                }
            }
            return emptyPerson();
        } else {
            return emptyPerson();
        }
    }

}
