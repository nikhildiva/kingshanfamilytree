package com.blackhawk.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.function.UnaryOperator;

@Slf4j
public class EnumUtils {

    /**
     * This method does a case insensitive conversion of string to enum
     *
     * @param enumClass                      target enum object class
     * @param value                          value to be parsed
     * @param optionalTransformationFunction if any other changes to be made for input value
     * @param <T>                            Type of target enum class
     * @return enum of type T on success, null on failure
     */
    public static <T extends Enum<T>> T parseEnumFromString(Class<T> enumClass, String value, UnaryOperator<String> optionalTransformationFunction) {
        if (value == null) {
            throw new NullPointerException("Expected non null value to parse");
        }
        T enumVal = Arrays.stream(enumClass.getEnumConstants())
                .filter(enumConst -> optionalTransformationFunction.apply(value).equalsIgnoreCase(enumConst.name()))
                .findFirst()
                .orElse(null);

        if (enumVal == null) {
            throw new IllegalArgumentException(value + " is not a valid value for " + enumClass.getName() + " enum type");
        }

        return enumVal;
    }

    /**
     * Calls above method without transforming input string
     *
     * @param enumClass target enum object class
     * @param value     value to be parsed
     * @param <T>       Type of target enum class
     * @return enum of type T on success, null on failure
     */
    public static <T extends Enum<T>> T parseEnumFromString(Class<T> enumClass, String value) {
        return parseEnumFromString(enumClass, value, string -> string);
    }
}
