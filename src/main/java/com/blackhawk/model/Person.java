package com.blackhawk.model;

import com.blackhawk.constant.Gender;
import com.blackhawk.constant.Relation;
import com.blackhawk.modelSupport.Persons;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.util.*;

/**
 * Getters of Person objects in Person class will return empty person to get rid of NPEs elegantly
 */
@Data
@EqualsAndHashCode
public class Person implements Serializable {
    private String name;
    private Gender gender;
    @EqualsAndHashCode.Exclude
    private Person father;
    @EqualsAndHashCode.Exclude
    private Person mother;
    @EqualsAndHashCode.Exclude
    private Person spouse;
    @EqualsAndHashCode.Exclude
    private List<Person> children;


    public Person(String name, Gender gender) {
        this.name = Objects.requireNonNull(name);
        this.gender = Objects.requireNonNull(gender);
        // Children are added to female. male getter will refer to female instance getter
        if (gender == Gender.FEMALE) {
            children = new ArrayList<>();
        } else {
            children = null;
        }
    }

    public boolean setSpouse(Person spouse) {
        // To stick with the father/mother concept, restricting user to have straight couples. I am not a homophobe!
        // Due to same assuming exceptions like polyamory/polygamy are not applicable
        if (spouse.gender != this.gender && !Persons.isEmpty(spouse) && Persons.isEmpty(getSpouse())) {
            this.spouse=spouse;
            spouse.setSpouse(this);
            if (gender == Gender.FEMALE) {
                children = new ArrayList<>();
            }
            return true;
        }
        return false;
    }

    public Person getSpouse() {
        return Optional.ofNullable(spouse).orElse(Persons.emptyPerson());
    }

    public Person getFather() {
        return Optional.ofNullable(father).orElse(Persons.emptyPerson());
    }

    public Person getMother() {
        return Optional.ofNullable(mother).orElse(Persons.emptyPerson());
    }

    // to ensure we don't do two copies of children, male children getter will refer to female
    public List<Person> getChildren() {
        if (spouse != null) {
            if (gender == Gender.MALE) {
                return spouse.getChildren();
            } else {
                return new ArrayList<>(ObjectUtils.firstNonNull(children, Collections.emptyList()));
            }
        } else {
            return Collections.emptyList();
        }
    }


    public boolean addChild(Person child) {
        if (getGender() == Gender.MALE || getSpouse() == Persons.emptyPerson()) {
            return false;
        } else {
            child.setFather(this.spouse);
            child.setMother(this);
            return children.add(child);
        }
    }

    public List<Person> getRelatives(Relation relation) {
        return relation.getRelationFunction(this);
    }

    @Override
    public String toString() {
        if (!Persons.isEmpty(this)) {
            return "Person{" +
                    "name='" + getName() + '\'' +
                    ", gender=" + getGender() +
                    ", father=" + (getFather() == Persons.emptyPerson() ? "" : getFather().getName()) +
                    ", mother=" + (getMother() == Persons.emptyPerson() ? "" : getMother().getName()) +
                    ", spouse=" + (getSpouse() == Persons.emptyPerson() ? "" : getSpouse().getName()) +
                    ", children=" + getChildren() +
                    '}';
        } else {
            return "";
        }
    }
}


