package com.blackhawk.familytree;

import com.blackhawk.constant.Gender;
import com.blackhawk.model.Person;

public class KingShanFamilyTree {
    // King and queen has space in names. No escape chars mentioned in questions.
    // So assuming tests will not have King and queen.
    // also we all know all-father is Odin!
    public static final Person allFather = new Person("King Shan", Gender.MALE);
    public static final Person allMother = new Person("Queen Anga", Gender.FEMALE);


    private KingShanFamilyTree() {
    }

    static {
        allFather.setSpouse(allMother);

        //children of King
        Person chit = new Person("Chit", Gender.MALE);
        allMother.addChild(chit);

        allMother.addChild(new Person("Ish", Gender.MALE));

        Person vich = new Person("Vich", Gender.MALE);
        allMother.addChild(vich);

        Person aras = new Person("Aras", Gender.MALE);
        allMother.addChild(aras);

        Person satya = new Person("Satya", Gender.FEMALE);
        allMother.addChild(satya);

        //spouses of king's children
        Person amba = new Person("Amba", Gender.FEMALE);
        chit.setSpouse(amba);
        Person lika = new Person("Lika", Gender.FEMALE);
        vich.setSpouse(lika);
        Person chitra = new Person("Chitra", Gender.FEMALE);
        aras.setSpouse(chitra);
        satya.setSpouse(new Person("Vyan", Gender.MALE));

        //children of chit
        Person dritha = new Person("Dritha", Gender.FEMALE);
        amba.addChild(dritha);

        amba.addChild(new Person("Tritha", Gender.FEMALE));
        amba.addChild(new Person("Vritha", Gender.MALE));

        dritha.setSpouse(new Person("Jaya", Gender.MALE));

        //child of dritha
        dritha.addChild(new Person("Yodhan", Gender.MALE));

        //children of vich
        lika.addChild(new Person("Vila", Gender.FEMALE));
        lika.addChild(new Person("Chika", Gender.FEMALE));

        //Children of aras
        Person jnki = new Person("Jnki", Gender.FEMALE);
        chitra.addChild(jnki);
        chitra.addChild(new Person("Ahit", Gender.MALE));

        //Children of jnki
        jnki.setSpouse(new Person("Arit", Gender.MALE));
        jnki.addChild(new Person("Laki", Gender.MALE));
        jnki.addChild(new Person("Lavnya", Gender.FEMALE));

        //Children of satya
        Person asva = new Person("Asva", Gender.MALE);
        satya.addChild(asva);

        Person vyas = new Person("Vyas", Gender.MALE);
        satya.addChild(vyas);

        satya.addChild(new Person("Atya", Gender.FEMALE));

        //spouse of satya's children
        Person satvy = new Person("Satvy", Gender.FEMALE);
        asva.setSpouse(satvy);
        Person krpi = new Person("Krpi", Gender.FEMALE);
        vyas.setSpouse(krpi);

        //children on kids of satya
        satvy.addChild(new Person("Vasa", Gender.MALE));

        krpi.addChild(new Person("Kriya", Gender.MALE));
        krpi.addChild(new Person("Krithi", Gender.FEMALE));
    }

    Person getAllFather() {
        return allFather;
    }
}
