package com.blackhawk.constant;

public enum QueryOutput {
    PERSON_NOT_FOUND,
    CHILD_ADDITION_SUCCEEDED,
    CHILD_ADDITION_FAILED,
    NONE;
}
