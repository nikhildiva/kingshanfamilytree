package com.blackhawk.constant;

import com.blackhawk.model.Person;

import java.util.List;

@FunctionalInterface
public interface FetchRelations {

    List<Person> apply(Person person);
}
