package com.blackhawk.constant;

public enum Gender {
    MALE,
    FEMALE
}
