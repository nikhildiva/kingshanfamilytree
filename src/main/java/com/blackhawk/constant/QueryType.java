package com.blackhawk.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum QueryType {
    ADD_CHILD,
    GET_RELATIONSHIP;
}
