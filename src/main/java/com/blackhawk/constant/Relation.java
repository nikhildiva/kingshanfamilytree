package com.blackhawk.constant;

import com.blackhawk.model.Person;
import com.blackhawk.modelSupport.Persons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public enum Relation {

    MOTHER(
            person -> Collections.singletonList(person.getMother())
    ),
    FATHER(
            person -> Collections.singletonList(person.getFather())
    ),
    MATERNAL_UNCLE(
            person -> {
                Person mother = person.getMother();
                return Persons.getSiblingsAsStream(mother)
                        .filter(sibling -> sibling.getGender() == Gender.MALE)
                        .collect(Collectors.toList());
            }
    ),
    MATERNAL_AUNT(
            person -> {
                Person mother = person.getMother();
                return Persons.getSiblingsAsStream(mother)
                        .filter(sibling -> sibling.getGender() == Gender.FEMALE)
                        .collect(Collectors.toList());
            }
    ),
    PATERNAL_UNCLE(
            person -> {
                Person father = person.getFather();
                return Persons.getSiblingsAsStream(father)
                        .filter(sibling -> sibling.getGender() == Gender.MALE)
                        .collect(Collectors.toList());
            }
    ),
    PATERNAL_AUNT(
            person -> {
                Person father = person.getFather();
                return Persons.getSiblingsAsStream(father)
                        .filter(sibling -> sibling.getGender() == Gender.FEMALE)
                        .collect(Collectors.toList());
            }
    ),
    SISTER_IN_LAW(
            person -> {
                Person spouse = person.getSpouse();
                List<Person> sisterInLaws = new ArrayList<>();
                if (!spouse.equals(Persons.emptyPerson()) && !spouse.getFather().getChildren().isEmpty()) {
                    sisterInLaws.addAll(Persons.getSiblingsAsStream(spouse)
                            .filter(sibling -> sibling.getGender() == Gender.FEMALE)
                            .collect(Collectors.toList()));

                }

                List<Person> siblings = Persons.getSiblingsAsStream(person)
                        .collect(Collectors.toList());

                siblings.forEach(
                        sibling -> {
                            Person siblingSpouse = sibling.getSpouse();
                            if (!Persons.isEmpty(siblingSpouse) && siblingSpouse.getGender() == Gender.FEMALE) {
                                sisterInLaws.add(siblingSpouse);
                            }
                        }
                );
                return sisterInLaws;
            }
    ),
    BROTHER_IN_LAW(
            person -> {
                Person spouse = person.getSpouse();
                List<Person> brotherInLaws = new ArrayList<>();
                if (!Persons.isEmpty(spouse) && !spouse.getFather().getChildren().isEmpty()) {
                    brotherInLaws.addAll(Persons.getSiblingsAsStream(spouse)
                            .filter(sibling -> sibling.getGender() == Gender.MALE)
                            .collect(Collectors.toList()));

                }

                List<Person> siblings = Persons.getSiblingsAsStream(person)
                        .collect(Collectors.toList());

                siblings.forEach(
                        sibling -> {
                            if (!Persons.isEmpty(sibling.getSpouse()) && sibling.getSpouse().getGender() == Gender.MALE) {
                                brotherInLaws.add(sibling.getSpouse());
                            }
                        }
                );
                return brotherInLaws;
            }
    ),
    SON(person ->
            person.getChildren()
                    .stream()
                    .filter(child -> child.getGender() == Gender.MALE)
                    .collect(Collectors.toList())
    ),
    DAUGHTER(person ->
            person.getChildren()
                    .stream()
                    .filter(child -> child.getGender() == Gender.FEMALE)
                    .collect(Collectors.toList())
    ),
    SIBLINGS(
            person ->
                    Persons.getSiblingsAsStream(person)
                            .collect(Collectors.toList())
    );


    Relation(FetchRelations fetchRelations) {
        this.fetchRelations = fetchRelations;
    }

    private final FetchRelations fetchRelations;

    public List<Person> getRelationFunction(Person person) {
        return fetchRelations.apply(person);
    }

}
