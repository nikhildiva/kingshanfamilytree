package com.blackhawk.factory.query;

import com.blackhawk.constant.QueryType;
import com.blackhawk.utils.EnumUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QueryFactory {

    private QueryFactory(){}

    public static Query getQuery(String queryString) {
        QueryType query;
        try {
            query = EnumUtils.parseEnumFromString(QueryType.class, queryString);
        } catch (NullPointerException | IllegalArgumentException e) {
            return null;
        }
        switch (query) {
            case ADD_CHILD:
                return new AddChildQuery();
            case GET_RELATIONSHIP:
                return new GetRelationsQuery();
            default:
                log.error("Action: {} not supported yet", query);
                return null;
        }
    }
}
