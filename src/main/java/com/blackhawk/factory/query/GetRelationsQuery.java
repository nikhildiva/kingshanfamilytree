package com.blackhawk.factory.query;

import com.blackhawk.constant.QueryOutput;
import com.blackhawk.constant.Relation;
import com.blackhawk.model.Person;
import com.blackhawk.modelSupport.Persons;
import com.blackhawk.utils.EnumUtils;

import java.util.List;
import java.util.stream.Collectors;

public class GetRelationsQuery extends Query {

    @Override
    public String performQuery(Person allFather, String[] brokenCommand) {
        int minArgs = 2;
        if (isArgCountInvalid(minArgs, brokenCommand)) {
            return null;
        }
        Person person = Persons.findInTree(allFather, brokenCommand[1]);
        if (Persons.isEmpty(person)) {
            return QueryOutput.PERSON_NOT_FOUND.name();
        }
        Relation relation = EnumUtils.parseEnumFromString(Relation.class, brokenCommand[2], string -> string.replace("-", "_"));
        List<Person> results = person.getRelatives(relation);
        if (results.isEmpty()) {
            return QueryOutput.NONE.name();
        } else {
            return results.stream()
                    .map(Person::getName)
                    .collect(Collectors.joining(" "));
        }
    }
}
