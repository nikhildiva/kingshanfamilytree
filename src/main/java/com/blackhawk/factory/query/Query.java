package com.blackhawk.factory.query;

import com.blackhawk.model.Person;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class Query {
    public abstract String performQuery(Person allFather, String[] brokenCommand);

    boolean isArgCountInvalid(int minArgs, String[] brokenCommand) {
        int argCount = brokenCommand.length - 1;
        if (argCount >= minArgs) {
            return false;
        } else {
            log.error("Expected minimum {} arguments for action {}. Received too less: {}", minArgs, brokenCommand[0], argCount);
            return true;
        }
    }
}
