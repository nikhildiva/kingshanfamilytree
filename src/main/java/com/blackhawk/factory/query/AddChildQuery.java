package com.blackhawk.factory.query;

import com.blackhawk.constant.Gender;
import com.blackhawk.constant.QueryOutput;
import com.blackhawk.model.Person;
import com.blackhawk.modelSupport.Persons;
import com.blackhawk.utils.EnumUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AddChildQuery extends Query {
    @Override
    public String performQuery(Person allFather, String[] brokenCommand) {

        int minArgs = 3;
        if (isArgCountInvalid(minArgs, brokenCommand)) {
            return null;
        }

        String motherName = brokenCommand[1];
        String childName = brokenCommand[2];
        Gender childGender;
        try {
            childGender = EnumUtils.parseEnumFromString(Gender.class, brokenCommand[3]);
        } catch (NullPointerException | IllegalArgumentException e) {
            return null;
        }
        Person child = new Person(childName, childGender);
        Person mother = Persons.findInTree(allFather, motherName);
        if (Persons.isEmpty(mother)) {
            return QueryOutput.PERSON_NOT_FOUND.name();
        } else if (mother.addChild(child)) {
            return QueryOutput.CHILD_ADDITION_SUCCEEDED.name();
        } else {
            return QueryOutput.CHILD_ADDITION_FAILED.name();
        }
    }
}
