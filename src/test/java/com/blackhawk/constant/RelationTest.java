package com.blackhawk.constant;

import com.blackhawk.model.Person;
import com.blackhawk.modelSupport.Persons;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class RelationTest {

    private static final Person testAllFather = new Person("testAllFather", Gender.MALE);
    public static final Person testAllMother = new Person("testAllMother", Gender.FEMALE);

    private static final Person paternalFather = new Person("fatherForPaternalTest", Gender.MALE);
    private static final Person paternalMother = new Person("motherForPaternalTest", Gender.FEMALE);
    private static final Person maternalFather = new Person("fatherForMaternalTest", Gender.MALE);
    private static final Person maternalMother = new Person("motherForMaternalTest", Gender.FEMALE);
    private static final Person AUNT_1 = new Person("Aunt1", Gender.FEMALE);
    private static final Person AUNT_2 = new Person("Aunt2", Gender.FEMALE);
    private static final Person UNCLE_1 = new Person("Uncle1", Gender.MALE);
    private static final Person UNCLE_2 = new Person("Uncle2", Gender.MALE);
    private static final Person paternalTestPerson = new Person("paternalTestPerson", Gender.MALE);
    private static final Person maternalTestPerson = new Person("maternalTestPerson", Gender.MALE);


    private static final Person SIL_1 = new Person("Sil1", Gender.FEMALE);
    private static final Person SIL_2 = new Person("Sil2", Gender.FEMALE);
    private static final Person BIL_1 = new Person("Bil1", Gender.MALE);
    private static final Person BIL_2 = new Person("Bil2", Gender.MALE);

    @BeforeClass
    public static void setup() {
        testAllFather.setSpouse(testAllMother);


        testAllMother.addChild(paternalFather);
        testAllMother.addChild(maternalMother);
        testAllMother.addChild(AUNT_1);
        testAllMother.addChild(UNCLE_1);
        testAllMother.addChild(AUNT_2);
        testAllMother.addChild(UNCLE_2);

        paternalFather.setSpouse(paternalMother);
        paternalMother.addChild(paternalTestPerson);

        maternalFather.setSpouse(maternalMother);
        maternalMother.addChild(maternalTestPerson);

        AUNT_1.setSpouse(BIL_1);
        AUNT_2.setSpouse(BIL_2);

        UNCLE_1.setSpouse(SIL_1);
        UNCLE_2.setSpouse(SIL_2);

    }

    @Test
    public void checkParentsAreAdded() {
        List<Person> allMotherList = paternalFather.getRelatives(Relation.MOTHER);
        Assert.assertEquals(1, allMotherList.size());
        Assert.assertEquals(testAllFather.getSpouse(), allMotherList.get(0));

        List<Person> allFatherList = paternalFather.getRelatives(Relation.FATHER);
        Assert.assertEquals(1, allFatherList.size());
        Assert.assertEquals(testAllFather, allFatherList.get(0));
    }

    @Test
    public void checkChildren() {
        //sons
        Assert.assertEquals(Arrays.asList(paternalFather, UNCLE_1, UNCLE_2), testAllFather.getRelatives(Relation.SON));
        Assert.assertEquals(Arrays.asList(paternalFather, UNCLE_1, UNCLE_2), testAllFather.getSpouse().getRelatives(Relation.SON));

        //daughters
        Assert.assertEquals(Arrays.asList(maternalMother, AUNT_1, AUNT_2), testAllFather.getRelatives(Relation.DAUGHTER));
        Assert.assertEquals(Arrays.asList(maternalMother, AUNT_1, AUNT_2), testAllFather.getSpouse().getRelatives(Relation.DAUGHTER));
    }

    @Test
    public void checkSiblings() {
        //ensure siblings are returned in order
        Assert.assertEquals(Arrays.asList(paternalFather, AUNT_1, UNCLE_1, AUNT_2, UNCLE_2), maternalMother.getRelatives(Relation.SIBLINGS));

        //ensure no siblings for persons married into family tree
        Assert.assertEquals(Collections.emptyList(), paternalMother.getRelatives(Relation.SIBLINGS));
    }

    @Test
    public void checkMaternalUnclesAreAdded() {
        List<Person> maternalUncles = maternalTestPerson.getRelatives(Relation.MATERNAL_UNCLE);

        //to ensure order of insertion is preserved
        Assert.assertEquals(Arrays.asList(paternalFather, UNCLE_1, UNCLE_2), maternalUncles);
    }


    @Test
    public void checkMaternalAuntAreAdded() {
        List<Person> maternalAunts = maternalTestPerson.getRelatives(Relation.MATERNAL_AUNT);

        //to ensure order of insertion is preserved
        Assert.assertEquals(Arrays.asList(AUNT_1, AUNT_2), maternalAunts);
    }

    @Test
    public void checkNoPaternalRelativesForMaternalTestPerson() {
        List<Person> paternalUncles = maternalTestPerson.getRelatives(Relation.PATERNAL_UNCLE);
        List<Person> paternalAunts = maternalTestPerson.getRelatives(Relation.PATERNAL_AUNT);

        Assert.assertEquals(Collections.emptyList(), paternalUncles);
        Assert.assertEquals(Collections.emptyList(), paternalAunts);
    }

    @Test
    public void checkPaternalUnclesAreAdded() {
        List<Person> paternalUncles = paternalTestPerson.getRelatives(Relation.PATERNAL_UNCLE);

        Assert.assertEquals(Arrays.asList(UNCLE_1, UNCLE_2), paternalUncles);
    }


    @Test
    public void checkPaternalAuntAreAdded() {
        List<Person> paternalAunts = paternalTestPerson.getRelatives(Relation.PATERNAL_AUNT);

        //to ensure order of insertion is preserved
        Assert.assertEquals(Arrays.asList(maternalMother, AUNT_1, AUNT_2), paternalAunts);
    }

    @Test
    public void checkNoMaternalRelativesForPaternalTestPerson() {
        List<Person> maternalUncles = paternalTestPerson.getRelatives(Relation.MATERNAL_UNCLE);
        List<Person> maternalAunts = paternalTestPerson.getRelatives(Relation.MATERNAL_AUNT);

        //to ensure order of insertion is preserved
        Assert.assertEquals(Collections.emptyList(), maternalUncles);
        Assert.assertEquals(Collections.emptyList(), maternalAunts);
    }

    @Test
    public void checkSisterInLawsAreAddedForDaughterAndDaughterHusband() {
        List<Person> daughterSisterInLaws = maternalMother.getRelatives(Relation.SISTER_IN_LAW);
        List<Person> daughterHusbandSisterInLaws = maternalFather.getRelatives(Relation.SISTER_IN_LAW);

        Assert.assertEquals(Arrays.asList(paternalMother, SIL_1, SIL_2), daughterSisterInLaws);
        Assert.assertEquals(Arrays.asList(AUNT_1, AUNT_2), daughterHusbandSisterInLaws);
    }

    @Test
    public void checkSisterInLawsAreAddedForSonAndSonWife() {
        List<Person> sonSisterInLaws = paternalFather.getRelatives(Relation.SISTER_IN_LAW);
        List<Person> sonWifeSisterInLaws = paternalMother.getRelatives(Relation.SISTER_IN_LAW);

        Assert.assertEquals(Arrays.asList(SIL_1, SIL_2), sonSisterInLaws);
        Assert.assertEquals(Arrays.asList(maternalMother, AUNT_1, AUNT_2), sonWifeSisterInLaws);
    }

    @Test
    public void checkBrotherInLawsAreAddedForDaughterAndDaughterHusband() {
        List<Person> daughterBrotherInLaws = maternalMother.getRelatives(Relation.BROTHER_IN_LAW);
        List<Person> daughterHusbandBrotherInLaws = maternalFather.getRelatives(Relation.BROTHER_IN_LAW);

        Assert.assertEquals(Arrays.asList(BIL_1, BIL_2), daughterBrotherInLaws);
        Assert.assertEquals(Arrays.asList(paternalFather, UNCLE_1, UNCLE_2), daughterHusbandBrotherInLaws);
    }

    @Test
    public void checkBrotherInLawsAreAddedForSonAndSonWife() {
        List<Person> sonBrotherInLaws = paternalFather.getRelatives(Relation.BROTHER_IN_LAW);
        List<Person> sonWifeBrotherInLaws = paternalMother.getRelatives(Relation.BROTHER_IN_LAW);

        Assert.assertEquals(Arrays.asList(maternalFather, BIL_1, BIL_2), sonBrotherInLaws);
        Assert.assertEquals(Arrays.asList(UNCLE_1, UNCLE_2), sonWifeBrotherInLaws);
    }

    @Test
    public void checkPersonSearch() {
        Assert.assertEquals(paternalTestPerson, Persons.findInTree(testAllFather, paternalTestPerson.getName()));
        Assert.assertEquals(paternalTestPerson, Persons.findInTree(paternalFather, paternalTestPerson.getName()));
    }

}