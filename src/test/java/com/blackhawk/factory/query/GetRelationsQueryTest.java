package com.blackhawk.factory.query;

import com.blackhawk.constant.Gender;
import com.blackhawk.constant.QueryOutput;
import com.blackhawk.model.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(JUnit4.class)
public class GetRelationsQueryTest {
    //Test data
    private static final Person testAllFather = new Person("testAllFather", Gender.MALE);
    public static final Person testAllMother = new Person("testAllMother", Gender.FEMALE);

    private static final Person paternalFather = new Person("fatherForPaternalTest", Gender.MALE);
    private static final Person paternalMother = new Person("motherForPaternalTest", Gender.FEMALE);
    private static final Person maternalFather = new Person("fatherForMaternalTest", Gender.MALE);
    private static final Person maternalMother = new Person("motherForMaternalTest", Gender.FEMALE);
    private static final Person AUNT_1 = new Person("Aunt1", Gender.FEMALE);
    private static final Person AUNT_2 = new Person("Aunt2", Gender.FEMALE);
    private static final Person UNCLE_1 = new Person("Uncle1", Gender.MALE);
    private static final Person UNCLE_2 = new Person("Uncle2", Gender.MALE);
    private static final Person paternalTestPerson = new Person("paternalTestPerson", Gender.MALE);
    private static final Person maternalTestPerson = new Person("maternalTestPerson", Gender.MALE);


    private static final Person SIL_1 = new Person("Sil1", Gender.FEMALE);
    private static final Person SIL_2 = new Person("Sil2", Gender.FEMALE);
    private static final Person BIL_1 = new Person("Bil1", Gender.MALE);
    private static final Person BIL_2 = new Person("Bil2", Gender.MALE);

    public static final Query getRelationsQuery = new GetRelationsQuery();

    @BeforeClass
    public static void setup() {
        testAllFather.setSpouse(testAllMother);


        testAllMother.addChild(paternalFather);
        testAllMother.addChild(maternalMother);
        testAllMother.addChild(AUNT_1);
        testAllMother.addChild(UNCLE_1);
        testAllMother.addChild(AUNT_2);
        testAllMother.addChild(UNCLE_2);

        paternalFather.setSpouse(paternalMother);
        paternalMother.addChild(paternalTestPerson);

        maternalFather.setSpouse(maternalMother);
        maternalMother.addChild(maternalTestPerson);

        AUNT_1.setSpouse(BIL_1);
        AUNT_2.setSpouse(BIL_2);

        UNCLE_1.setSpouse(SIL_1);
        UNCLE_2.setSpouse(SIL_2);
    }

    //Positive tests
    @Test
    public void getRelationsPositiveQuery() {
        String expectedOutput = getNamesFromPersons(maternalMother, AUNT_1, UNCLE_1, AUNT_2, UNCLE_2);
        Assert.assertEquals(expectedOutput, getRelationsQuery.performQuery(testAllFather, new String[]{"GET_RELATIONSHIP", "fatherForPaternalTest", "siblings"}));
        Assert.assertEquals(expectedOutput, getRelationsQuery.performQuery(testAllFather, new String[]{"GET_RELATIONSHIP", "fatherForPaternalTest", "Siblings"}));
    }

    @Test
    public void getRelationsWithEmptyResult(){
        Assert.assertEquals(QueryOutput.NONE.name(), getRelationsQuery.performQuery(testAllFather, new String[]{"GET_RELATIONSHIP", "paternalTestPerson", "Siblings"}));
    }

    // Negatove tests
    @Test
    public void getRelationsWithInvalidPerson(){
        Assert.assertEquals(QueryOutput.PERSON_NOT_FOUND.name(), getRelationsQuery.performQuery(testAllFather, new String[]{"GET_RELATIONSHIP", "InvalidPerson", "Siblings"}));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getRelationsWithInvalidRelation(){
        getRelationsQuery.performQuery(testAllFather, new String[]{"GET_RELATIONSHIP", "fatherForPaternalTest", "InvalidReln"});
    }

    String getNamesFromPersons(Person... persons) {
        return Stream.of(persons)
                .map(Person::getName)
                .collect(Collectors.joining(" "));
    }


}