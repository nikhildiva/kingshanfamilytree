package com.blackhawk.factory.query;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class QueryFactoryTest {

    @Test
    public void checkGettingCorrectTypeWithValidInput() {
        Assert.assertTrue(QueryFactory.getQuery("add_child") instanceof AddChildQuery);
        Assert.assertTrue(QueryFactory.getQuery("ADD_CHILD") instanceof AddChildQuery);
    }

    @Test
    public void checkGettingNullWithInvalidInput() {
        Assert.assertNull(QueryFactory.getQuery("invalidQuery"));
    }

}