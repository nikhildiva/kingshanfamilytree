package com.blackhawk.factory.query;

import com.blackhawk.constant.Gender;
import com.blackhawk.constant.QueryOutput;
import com.blackhawk.model.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Collections;

@RunWith(JUnit4.class)
public class AddChildQueryTest {


    //Test data
    private static final Person testAllFather = new Person("testAllFather", Gender.MALE);
    public static final Person testAllMother = new Person("testAllMother", Gender.FEMALE);

    private static final Person paternalFather = new Person("fatherForPaternalTest", Gender.MALE);
    private static final Person paternalMother = new Person("motherForPaternalTest", Gender.FEMALE);
    private static final Person maternalFather = new Person("fatherForMaternalTest", Gender.MALE);
    private static final Person maternalMother = new Person("motherForMaternalTest", Gender.FEMALE);
    private static final Person AUNT_1 = new Person("Aunt1", Gender.FEMALE);
    private static final Person AUNT_2 = new Person("Aunt2", Gender.FEMALE);
    private static final Person UNCLE_1 = new Person("Uncle1", Gender.MALE);
    private static final Person UNCLE_2 = new Person("Uncle2", Gender.MALE);
    private static final Person paternalTestPerson = new Person("paternalTestPerson", Gender.MALE);
    private static final Person maternalTestPerson = new Person("maternalTestPerson", Gender.MALE);


    private static final Person SIL_1 = new Person("Sil1", Gender.FEMALE);
    private static final Person SIL_2 = new Person("Sil2", Gender.FEMALE);
    private static final Person BIL_1 = new Person("Bil1", Gender.MALE);
    private static final Person BIL_2 = new Person("Bil2", Gender.MALE);

    public static final Query addChildQuery = new AddChildQuery();

    @Before
    public void setup() {
        testAllFather.setSpouse(testAllMother);


        testAllMother.addChild(paternalFather);
        testAllMother.addChild(maternalMother);
        testAllMother.addChild(AUNT_1);
        testAllMother.addChild(UNCLE_1);
        testAllMother.addChild(AUNT_2);
        testAllMother.addChild(UNCLE_2);

        paternalFather.setSpouse(paternalMother);
        paternalMother.addChild(paternalTestPerson);

        maternalFather.setSpouse(maternalMother);
        maternalMother.addChild(maternalTestPerson);

        AUNT_1.setSpouse(BIL_1);
        AUNT_2.setSpouse(BIL_2);

        UNCLE_1.setSpouse(SIL_1);
        UNCLE_2.setSpouse(SIL_2);

    }


    //Add Children positive tests
    @Test
    public void addChildrenToCouple() {
        Assert.assertEquals(
                QueryOutput.CHILD_ADDITION_SUCCEEDED.name(),
                addChildQuery.performQuery(testAllFather, new String[]{"ADD_CHILD", "Aunt2", "child1", "male"})
        );
        Assert.assertEquals(
                QueryOutput.CHILD_ADDITION_SUCCEEDED.name(),
                addChildQuery.performQuery(testAllFather, new String[]{"ADD_CHILD", "Aunt2", "child2", "female"})
        );

        Assert.assertEquals(2, BIL_2.getChildren().size());
        Person child1 = BIL_2.getChildren().get(0);
        Person child2 = BIL_2.getChildren().get(1);

        Assert.assertEquals("child1", child1.getName());
        Assert.assertEquals(Gender.MALE, child1.getGender());

        Assert.assertEquals("child2", child2.getName());
        Assert.assertEquals(Gender.FEMALE, child2.getGender());
    }

    //Add Children negative tests

    @Test
    public void addChildrenToMale(){
        Assert.assertEquals(
                QueryOutput.CHILD_ADDITION_FAILED.name(),
                addChildQuery.performQuery(testAllFather, new String[]{"ADD_CHILD", "Bil2", "child1", "female"})
        );

        Assert.assertEquals(Collections.emptyList(), BIL_2.getChildren());
    }

    @Test
    public void addChildToSingle(){
        Assert.assertEquals(
                QueryOutput.CHILD_ADDITION_FAILED.name(),
                addChildQuery.performQuery(testAllFather, new String[]{"ADD_CHILD", "paternalTestPerson", "child1", "female"})
        );

        Assert.assertEquals(Collections.emptyList(), paternalTestPerson.getChildren());
    }

    @Test
    public void addChildrenToInvalidPerson() {
        Assert.assertEquals(
                QueryOutput.PERSON_NOT_FOUND.name(),
                addChildQuery.performQuery(testAllFather, new String[]{"ADD_CHILD", "Invalid Person", "child1", "male"})
        );
    }




}