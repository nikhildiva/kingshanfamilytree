package com.blackhawk.model;

import com.blackhawk.constant.Gender;
import com.blackhawk.modelSupport.Persons;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class PersonTest {


    //positive init test

    @Test
    public void emptyPersonForNewPersonTest() {
        Person person = new Person("test", Gender.MALE);

        Assert.assertEquals("test", person.getName());
        Assert.assertSame(Gender.MALE, person.getGender());
        Assert.assertTrue(Persons.isEmpty(person.getFather()));
        Assert.assertTrue(Persons.isEmpty(person.getMother()));
        Assert.assertTrue(Persons.isEmpty(person.getSpouse()));
        Assert.assertSame(Collections.<Person>emptyList(), person.getChildren());
    }

    //negative init test
    @Test(expected = NullPointerException.class)
    public void initPersonWithNullNameTest() {
        new Person(null, Gender.MALE);
    }

    @Test(expected = NullPointerException.class)
    public void initPersonWithNullGenderTest() {
        new Person("test", null);
    }

    //positive set spouse test
    @Test
    public void setSpouseTest() {
        Person male = new Person("testMale", Gender.MALE);
        Person female = new Person("testFemale", Gender.FEMALE);

        Assert.assertTrue(male.setSpouse(female));
        Assert.assertEquals(male, female.getSpouse());
        Assert.assertEquals(female, male.getSpouse());
        Assert.assertEquals(female.getChildren(), male.getChildren());
    }


    //negative set spouse test
    @Test
    public void setSameSexSpouse() {
        Person male1 = new Person("testMale1", Gender.MALE);
        Person male2 = new Person("testMale2", Gender.MALE);

        Assert.assertFalse(male1.setSpouse(male2));
        Assert.assertTrue(Persons.isEmpty(male1.getSpouse()));
        Assert.assertEquals(Collections.emptyList(), male1.getChildren());
    }


    @Test
    public void setSpouseTwice() {
        Person male = new Person("testMale", Gender.MALE);
        Person female = new Person("testFemale", Gender.FEMALE);
        Person female2 = new Person("testFemale2", Gender.FEMALE);

        male.setSpouse(female);

        Assert.assertFalse(male.setSpouse(female2));
        Assert.assertEquals(female, male.getSpouse());
    }

    @Test
    public void setEmptySpouse() {
        Person male = new Person("testMale", Gender.MALE);

        Assert.assertFalse(male.setSpouse(Persons.emptyPerson()));
        Assert.assertEquals(Persons.emptyPerson(), male.getSpouse());
    }

    //positive add child test
    @Test
    public void addChildToCouple() {
        Person male = new Person("male", Gender.MALE);
        Person female = new Person("female", Gender.FEMALE);
        Person child1 = new Person("child1", Gender.MALE);
        Person child2 = new Person("child2", Gender.MALE);


        male.setSpouse(female);

        female.addChild(child1);
        female.addChild(child2);
        //ensuring mom and dad refers to same child
        // ensuring order is preserved
        Assert.assertEquals(Arrays.asList(child1, child2), male.getChildren());
    }

    //negative add child test
    @Test
    public void addChildToSingle() {
        Person male = new Person("male", Gender.MALE);
        Person child = new Person("child", Gender.MALE);

        Assert.assertFalse(male.addChild(child));
        Assert.assertEquals(Collections.<Person>emptyList(), male.getChildren());
    }

    // father or mather can't modify child by List.add negative test
    @Test
    public void addChildByFather(){
        Person male = new Person("male", Gender.MALE);
        Person female = new Person("male", Gender.FEMALE);
        Person child1 = new Person("child", Gender.MALE);
        Person child2 = new Person("child", Gender.FEMALE);


        male.setSpouse(female);

        female.addChild(child1);

        female.getChildren().add(child2);

        male.getChildren().add(child2);

        Assert.assertEquals(Collections.singletonList(child1),male.getChildren());
        Assert.assertEquals(Collections.singletonList(child1),female.getChildren());
    }
}